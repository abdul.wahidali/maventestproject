
import com.aventstack.extentreports.utils.FileUtil;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestingInterviewQuestions {
    static WebDriver driver;

    @Test
    public void interviewQuestions() throws IOException {
        List<String> urls = new ArrayList();
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--disable-notifications");
        option.addArguments("start-maximized");
        option.addArguments("--disable-extensions");
        urls.add("https://www.softwaretestingo.com/cts-cognizant-interview-questions/");
        urls.add("https://www.softwaretestingo.com/capgemini-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/tech-mahindra-tech-m-interview-questions/");
        urls.add("https://www.softwaretestingo.com/wipro-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/mphasis-interview-questions-process/");
        urls.add("https://www.softwaretestingo.com/infosys-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/tcs-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/accenture-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/restful-api-web-services-interview-questions/");
        urls.add("https://www.softwaretestingo.com/tavant-automation-questions/");
        urls.add("https://www.softwaretestingo.com/adobe-automation-interview-questions/");
        urls.add("https://www.softwaretestingo.com/goldman-interview-questions/");
        urls.add("https://www.softwaretestingo.com/morgan-stanley-interview-questions/");
        urls.add("https://www.softwaretestingo.com/basic-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/testng-interview-questions/");
        urls.add("https://www.softwaretestingo.com/java-interview-questions/");
        urls.add("https://www.softwaretestingo.com/harman-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/appium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/citi-bank-interview-questions/");
        urls.add("https://www.softwaretestingo.com/synechron-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/devops-interview-questions/");
        urls.add("https://www.softwaretestingo.com/deloitte-automation-interview-question/");
        urls.add("https://www.softwaretestingo.com/huron-interview-questions/");
        urls.add("https://www.softwaretestingo.com/globallogic-interview-questions/");
        urls.add("https://www.softwaretestingo.com/hp-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/sapient-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/fis-interview-questions/");
        urls.add("https://www.softwaretestingo.com/accolite-automation-questions/");
        urls.add("https://www.softwaretestingo.com/qualitest-automation-testing-questions/");
        urls.add("https://www.softwaretestingo.com/microsoft-interview-questions/");
        urls.add("https://www.softwaretestingo.com/hcl-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/ibs-interview-questions/");
        urls.add("https://www.softwaretestingo.com/infogain-selenium-interview-questions/");
        urls.add("https://www.softwaretestingo.com/excellere-interview-questions/");
        urls.add("https://www.softwaretestingo.com/exilant-technology-quest-questions/");
        File fr = null;
        File qFr = null;
        System.setProperty("webdriver.chrome.driver", "D:\\SampleProjects\\MavenProject\\drivers\\chromeDriver\\chromedriver.exe");
        driver = new ChromeDriver(option);
        //driver.manage().window().maximize();
        try {
            fr = new File("C:\\Users\\HP\\Desktop\\Companies Interview Question.txt");
            qFr = new File("C:\\Users\\HP\\Desktop\\Interview Questions URLs.txt");
            FileUtils.writeStringToFile(fr, "");
            FileUtils.writeStringToFile(qFr, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int sno=0;
        for (String url : urls) {
            sno++;;
            driver.get(url);
            FileUtils.writeStringToFile(qFr, sno+". "+url+"\n", true);
            String header = driver.findElement(By.xpath("//header[@class=\"entry-header\"]/h1")).getText();
            List<WebElement> elements = driver.findElements(By.xpath("//div[@class=\"entry-content\"]/ul/li"));
            //driver.findElement(By.xpath("//header[@class=\"entry-header\"]/h1")).getScreenshot(OutputType.FILE);
            try {
                FileUtils.writeStringToFile(fr, "\n---------------------------------------------------------------------------------------------------------------------------------\n", true);
                FileUtils.writeStringToFile(fr, header, true);
                FileUtils.writeStringToFile(fr, "\n---------------------------------------------------------------------------------------------------------------------------------\n", true);
                //for (WebElement ele : elements) {
                for (int i = 1; i <= elements.size(); i++) {
                    String q = elements.get(i - 1).getText();
                    FileUtils.writeStringToFile(fr, i + ". " + q + "\n", true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        driver.quit();
    }

    @AfterMethod
    public void screenShot(ITestResult result) {
        if (ITestResult.FAILURE == result.getStatus()) {
            File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File des = new File("D:\\SampleProjects\\MavenProject\\screenshots\\failure.png");
            try {
                FileUtils.copyFile(src, des);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
